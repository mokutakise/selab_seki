package com.example.tsudzukitatsuya.walkingsmartphone;


import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;


import javax.net.ssl.HttpsURLConnection;

public class HttpResponsAsync extends AsyncTask<Float, Void, String> {

    private CallBackTask callbacktask;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // doInBackground前処理
    }

    @Override
    protected String doInBackground(Float... params) {
        HttpURLConnection con = null;
        URL url = null;
        String urlSt = "http://160.16.210.86:8080/users";

        Float light = params[0];
        Float weekday = params[1];
        Float hour = params[2];
        Float latitude = params[3];
        Float longitude = params[4];
        Float dangerplace = params[5];
        Float risk = params[6];
        Float dangertype = params[7];

        try {
            // URLの作成
            url = new URL(urlSt);
            // 接続用HttpURLConnectionオブジェクト作成
            con = (HttpURLConnection)url.openConnection();
            //接続タイムアウトを設定する。
            con.setConnectTimeout(100000);
            //レスポンスデータ読み取りタイムアウトを設定する。
            con.setReadTimeout(100000);
            //ヘッダーにUser-Agentを設定する。
            con.setRequestProperty("User-Agent", "Android");
            //ヘッダーにAccept-Languageを設定する。
            con.setRequestProperty("Accept-Language", Locale.getDefault().toString());
            //ヘッダーにContent-Typeを設定する
            con.addRequestProperty("Content-Type", "application/json; charset=UTF-8");
            // リクエストメソッドの設定
            con.setRequestMethod("POST");
            // リダイレクトを自動で許可しない設定
            con.setInstanceFollowRedirects(false);
            // URL接続からデータを読み取る場合はtrue
            con.setDoInput(true);
            // URL接続にデータを書き込む場合はtrue
            con.setDoOutput(true);


            // 接続
            con.connect(); // ①
            //ステップ5:リクエストボディの書き出しを行う。
            OutputStream outputStream = con.getOutputStream();
            HashMap<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("light" , light);
            jsonMap.put("weekday" , weekday);
            jsonMap.put("hour" , hour);
            jsonMap.put("latitude" , latitude);
            jsonMap.put("longitude" , longitude);
            jsonMap.put("weather" , -1);//weather);
            jsonMap.put("temperature" , 0); //temperature);
            jsonMap.put("dangerPlace" , dangerplace);
            jsonMap.put("risk" , risk);
            jsonMap.put("dangertype", dangertype);

            if (jsonMap.size() > 0) {
                //JSON形式の文字列に変換する。
                JSONObject responseJsonObject = new JSONObject(jsonMap);
                String jsonText = responseJsonObject.toString();
                PrintStream ps = new PrintStream(con.getOutputStream());
                ps.print(jsonText);
                ps.close();
            }
            outputStream.close();

            Log.d("connection", String.valueOf(con.getResponseCode()));

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (con != null){
                //コネクションを閉じる
                con.disconnect();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        callbacktask.CallBack(result);
        // doInBackground後処理
    }

    public void setOnCallBack(CallBackTask _cbj) {
        callbacktask = _cbj;
    }

    /**
     * コールバック用のstaticなclass
     */
    public static class CallBackTask {
        public void CallBack(String result) {
        }
    }
}