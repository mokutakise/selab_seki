package com.example.tsudzukitatsuya.walkingsmartphone;


import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.util.Log;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;
import android.Manifest;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.text.SpannableStringBuilder;

public class MainActivity extends AppCompatActivity implements SensorEventListener,LocationListener {


    //メンバー変数
    private SensorManager mSensorManager;              //センサーマネージャー
    private Sensor mLight;                             //光センサー
    private int day;                                   //日
    private float week;                                //曜日(登録データ)
    private float hour;                                //時間(登録データ)
    private int minute;                                //分
    private LocationManager mlocationManager;          //GPS
    private float light;                               //照度センサ(登録データ)
    private float latitude;                            //緯度(登録データ)
    private float longitude;                           //経度(登録データ)
    private float dangerplace;                         //危険場所の種別(登録データ)
    private float risk;                                //危険度(登録データ)
    private float dangertype;                          //危険種別(登録データ)



    //曜日の配列
    final static String[] WEEK = {"日", "月", "火", "水", "木", "金", "土"};

    TextView mTextOutputLight;               //光センサーの値を入れるtextview
    TextView mTextOutputDay;                 //日付を入れるtextview
    TextView mTextOutputDayOFTheWeek;        //曜日を入れるtextview
    TextView mTextOutputTime;                //現在時刻を入れるtextview

    private TextView textView;               //ボタン用
    private boolean flag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //結び付け
        mTextOutputLight = (TextView) findViewById(R.id.TextOutputLight);
        mTextOutputDay = (TextView) findViewById(R.id.TextOutputDay);
        mTextOutputDayOFTheWeek = (TextView) findViewById(R.id.TextOutputDayOfTheWeek);
        mTextOutputTime = (TextView) findViewById(R.id.TextOutputTime);

        //SensorManagerオブジェクトを取得
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //光センサーである[Sensor.TYPE_LIGHT]を指定
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        //日時表示メソッドの実行
        showDate();


        //危険場所の種別の取得(ラジオボタン)
        final Spinner spinner = (Spinner)findViewById(R.id.spinner);
        Log.d("debug_radio_dangerplace",String.valueOf(spinner));


        //危険度の取得(ラジオボタン)
        RadioGroup group3 = (RadioGroup)findViewById(R.id.radiogroup3);
        group3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group3, int checkedId) {
                // TODO Auto-generated method stub
                RadioButton radio3 = (RadioButton)findViewById(checkedId);
                if(radio3.isChecked() == true) {
                    // ェックされた状態の時の処理を記述
                    switch (checkedId) {
                        case R.id.radioButton9:
                            risk = 0;
                            break;
                        case R.id.radioButton10:
                            risk = 1;
                            break;
                        case R.id.radioButton11:
                            risk = 2;
                            break;
                    }
                }
                Log.d("debug_radio_risk",String.valueOf(risk));
            }
        });

        //危険種別の取得(ラジオボタン)
        RadioGroup group4 = (RadioGroup)findViewById(R.id.radiogroup4);
        group4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group4, int checkedId) {
                // TODO Auto-generated method stub
                RadioButton radio4 = (RadioButton)findViewById(checkedId);
                if(radio4.isChecked() == true) {
                    // ェックされた状態の時の処理を記述
                    switch (checkedId) {
                        case R.id.radioButton12:
                            dangertype = 0;
                            break;
                        case R.id.radioButton13:
                            dangertype = 1;
                            break;
                        case R.id.radioButton14:
                            dangertype = 2;
                            break;
                    }
                }
                Log.d("debug_radio_dangertype",String.valueOf(dangertype));
            }
        });

        //GPS
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("No Permisson", "!!!!!");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        }
        else{
            locationStart();
            mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, this);
        }


        // ボタンを設定
        Button button = (Button) findViewById(R.id.registerButton);

        // リスナーをボタンに登録
        // ボタンの動作を登録
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            //ボタンが押された時の処理
            public void onClick(View v) {
                Toast toast = Toast.makeText(MainActivity.this, "危険場所情報を登録しました!", Toast.LENGTH_SHORT);
                toast.show();

                try {
                    //入力情報の変換
                    HttpResponsAsync atClass = new HttpResponsAsync();
                    atClass.setOnCallBack(new HttpResponsAsync.CallBackTask(){

                        @Override
                        public void CallBack(String result) {
                            super.CallBack(result);
                            // ※１
                            // resultにはdoInBackgroundの返り値が入ります。
                            // ここからAsyncTask処理後の処理を記述します。
                            Log.i("AsyncTaskCallback", "非同期処理が終了しました。");
                        }
                    });

                    //選択されたspinner IDの取得と変換
                    dangerplace = spinner.getSelectedItemId();

                    Log.d("debug", String.valueOf(dangerplace));

                    // AsyncTaskの実行
                    Log.d("hour", String.valueOf(hour));
                    atClass.execute(light, week, hour, latitude, longitude, dangerplace, risk, dangertype);
                    Log.d("dangertype",String.valueOf(dangertype));
                } catch(NumberFormatException nf){
                    Log.d("Error", "NumberFormatException");
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        //registerListener()でSensorEventListenerを登録
        //光センサーの場合 SENSOR_DELAY_NORMALは[画面の向きの変更に最適な通常モード]
        mSensorManager.registerListener(this, mLight, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        super.onPause();
        //unregisterListener()で、SensorEventListenerの登録解除
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO 自動生成されたメソッド・スタブ
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        light = event.values[0];                                         //照度センサの値(グローバル)
        Log.d("debug_light", String.valueOf(light));

        if (event.sensor.getType() == Sensor.TYPE_LIGHT) {                    //光センサーを取得した場合
            //SensorEventのvalues配列をセットする(光の場合は1つしかない)
            mTextOutputLight.setText(String.valueOf(light));
        } else {
            return;
        }
    }

    /**
     * showDate()
     * 日時表示処理
     */
    private void showDate() {
        // TODO 自動生成されたメソッド・スタブ
        Calendar calendar = Calendar.getInstance();                     //カレンダーのインスタンス
        day = calendar.get(Calendar.DAY_OF_MONTH);                      //日
        week = calendar.get(Calendar.DAY_OF_WEEK);                      //曜日
        String[] weekName = {"日", "月", "火" ,"水" ,"木" ,"金" ,"土"};
        String weekView = "";
        hour = calendar.get(Calendar.HOUR_OF_DAY);                      //時間
        minute = calendar.get(Calendar.MINUTE);                         //分
        String hourTime = "";
        String minuteTime = "";
        int hourView = (int)hour;                                       //UI表示時のキャスト変換用

        hourTime = String.valueOf(hourView);                            //時間を取得
        minuteTime = String.valueOf(minute);                            //分を取得

        String time = hourTime + ":" + minuteTime;                      //HH:mm

        if (week == 1.0) {
            weekView = weekName[0];
        }
        else if(week == 2.0){
            weekView = weekName[1];
        }
        else if(week == 3.0){
            weekView = weekName[2];
        }
        else if(week == 4.0){
            weekView = weekName[3];
        }
        else if(week == 5.0){
            weekView = weekName[4];
        }
        else if(week == 6.0){
            weekView = weekName[5];
        }
        else if(week == 7.0){
            weekView = weekName[6];
        }

        //各TextViewにセット
        mTextOutputDay.setText(String.valueOf(day));
        mTextOutputDayOFTheWeek.setText(String.valueOf(weekView));
        mTextOutputTime.setText(time);



        //Log.d("debug_data_return", debugStr);
        Log.d("debug_week", String.valueOf(week));
        Log.d("debug_hour", String.valueOf(hourView));


        return ;

    }

    private void locationStart(){
        Log.d("debug","locationStart()");

        // LocationManager インスタンス生成
        mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (mlocationManager != null && mlocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d("debug", "location manager Enabled");
        } else {
            // GPSを設定するように促す
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
            Log.d("debug", "not gpsEnable, startActivity");
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

            Log.d("debug", "checkSelfPermission false");
            return;
        }

        mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, this);
        mlocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 50, this);
    }

    // 結果の受け取り
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            // 使用が許可された
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("debug","checkSelfPermission true");

                locationStart();

            } else {
                // それでも拒否された時の対応
                Toast toast = Toast.makeText(this, "これ以上なにもできません", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        latitude = (float) location.getLatitude();
        longitude = (float) location.getLongitude();
        Log.d("dubug_latitude", String.valueOf(latitude));
        Log.d("dubug_longitude", String.valueOf(longitude));

        // 緯度の表示
        TextView mTextOutputGpsx = (TextView) findViewById(R.id.TextOutputGpsx);
        mTextOutputGpsx.setText(String.valueOf(latitude));

        // 経度の表示
        TextView mTextOutputGpsy = (TextView) findViewById(R.id.TextOutputGpsy);
        mTextOutputGpsy.setText(String.valueOf(longitude));

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}