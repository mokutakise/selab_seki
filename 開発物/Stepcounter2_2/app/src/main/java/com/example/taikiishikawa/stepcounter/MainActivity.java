package com.example.taikiishikawa.stepcounter;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends Activity implements SensorEventListener,LocationListener {

    private SensorManager manager;
    private Sensor detectorSensor;
    private Sensor stepCntSensor;

    private int stepcount = 0;
    private int stepcount2 = 0;
    private int stepcountBefore5sec = 0;
    private int stepcount2Before5sec = 0;
    private NotificationManager mManager;
    private int number = 0;
    private int count = 0;
    private int countBefore = 0;
    private boolean beforestop = true;

    private TextView textview;
    private TextView textview2;
    private TextView textview3;
    private TextView textview4;

    private final MainActivity self = this;

    //メンバー変数
    private SensorManager mSensorManager;               //センサーマネージャー
    private Sensor mLight;                              //光センサー
    private int day;                                    //日
    private float week;                                   //曜日(登録データ)
    private float hour;                                   //時間(登録データ)
    private int minute;                                 //分
    private LocationManager mlocationManager;           //GPS
    private float light;                                //照度センサ(登録データ)
    private float latitude;                            //緯度(登録データ)
    private float longitude;                           //経度(登録データ)
    private float weather;                             //天気(登録データ)
    private float danger;                              //危険種別(登録データ)
    private float temperature;                         //気温(登録データ)
    private float risk;                                //危険度(登録データ)

    //曜日の配列
    final static String[] WEEK = {"日", "月", "火", "水", "木", "金", "土"};

    private void showDate() {
        // TODO 自動生成されたメソッド・スタブ
        Calendar calendar = Calendar.getInstance();            //カレンダーのインスタンス
        day = calendar.get(Calendar.DAY_OF_MONTH);             //日
        week = calendar.get(Calendar.DAY_OF_WEEK) - 1;         //曜日
        hour = calendar.get(Calendar.HOUR_OF_DAY);             //時間
        minute = calendar.get(Calendar.MINUTE);                //分


        String hourTime = "";
        String minuteTime = "";


        hourTime = String.valueOf(hour);                        //時間を取得
        minuteTime = String.valueOf(minute);                    //分を取得

        String time = hourTime + ":" + minuteTime;        //HH:mm
        return ;
    }
    private void locationStart(){
        Log.d("debug","locationStart()");

        // LocationManager インスタンス生成
        mlocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (mlocationManager != null && mlocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Log.d("debug", "location manager Enabled");
        } else {
            // GPSを設定するように促す
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
            Log.d("debug", "not gpsEnable, startActivity");
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);

            Log.d("debug", "checkSelfPermission false");
            return;
        }

        mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, this);

    }
    // 結果の受け取り
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            // 使用が許可された
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("debug","checkSelfPermission true");

                locationStart();

            } else {
                // それでも拒否された時の対応
                Toast toast = Toast.makeText(this, "これ以上なにもできません", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = (float) location.getLatitude();
        longitude = (float) location.getLongitude();
        Log.d("dubug_latitude", String.valueOf(latitude));
        Log.d("dubug_longitude", String.valueOf(longitude));
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    //ノーティフィケーション
    private void sendNotification() {
        mManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification n = new NotificationCompat.Builder(this)
                .setContentTitle("周囲に注意して歩きましょう。")
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .build();
        mManager.notify(number, n);
        number++;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //SensorManagerオブジェクトを取得
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //光センサーである[Sensor.TYPE_LIGHT]を指定
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        //GPS
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        }
        else{
            locationStart();
            mlocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 50, this);
        }

        // alertDialog
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            @Override
            public void run() {

                textview4.setText(stepcount + ":" + stepcountBefore5sec + ":" + stepcount2 + ":" + stepcount2Before5sec);
                if (stepcount > stepcountBefore5sec || stepcount2 > stepcount2Before5sec) {//歩行の判断
                    count++;
                    sendNotification();
                    textview3.setText("歩いてる");
                    // AsyncTaskの実行/現在位置の危険度判定する
                    HttpResponseAsync atClass = new HttpResponseAsync();
                    atClass.setOnCallBack(new HttpResponseAsync.CallBackTask() {

                        @Override
                        public void CallBack(String result) {
                            Log.d("aaaaa", String.valueOf(result == null));
                            super.CallBack(result);
                            // ※１
                            // resultにはdoInBackgroundの返り値が入ります。
                            // ここからAsyncTask処理後の処理を記述します。
                            Log.i("AsyncTaskCallback", "非同期処理が終了しました。" + "["+(result==null) + "]");
                            //危険かどうかの判定
                            if(result == "0"){
                                sendNotification();
                            }else{
                                Intent intentService = new Intent(self, TimerIntentService.class);
                                startService(intentService);//呼び出すとバックでいける
                            }
                        }
                    });
                    atClass.execute(light,week,hour,latitude,longitude);
                }else {
                    textview3.setText("歩いてない");
                    // 一回前のループでbeforestopがどうかを判定できる
                    if (beforestop == true) {
                        count = 0;
                    } else {
                        count = 0;
                    }
                    // 今回歩いていない、という情報を保存
                    beforestop = true;
                }
                stepcountBefore5sec = stepcount;
                stepcount2Before5sec = stepcount2;

                handler.postDelayed(this, 400);
            }
        };

        handler.post(r);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //センサーマネージャを取得
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //センサマネージャから TYPE_STEP_DETECTOR についての情報を取得する
        detectorSensor = manager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        //センサマネージャから TYPE_STEP_COUNTER についての情報を取得する
        stepCntSensor = manager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

        textview = (TextView) findViewById(R.id.textview);
        textview2 = (TextView) findViewById(R.id.textview2);
        textview3 = (TextView) findViewById(R.id.textview3);
        textview4 = (TextView) findViewById(R.id.textview4);

        textview.setText("STEP_DETECTOR=");
        textview2.setText("STEP_COUNTER=");
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // accuracy に変更があった時の処理
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor sensor = event.sensor;
        float[] values = event.values;
        long timestamp = event.timestamp;

        //TYPE_STEP_COUNTER
        if (sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            // 今までの歩数
            Log.d("type_step_counter", String.valueOf(values[0]));
            stepcount2++;
            textview2.setText("STEP_COUNTER=" + stepcount2 + "歩");
        }
        if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            // ステップを検知した場合にアクセス
            Log.d("type_detector_counter", String.valueOf(values[0]));
            stepcount++;
            textview.setText("STEP_DETECTOR=" + stepcount + "歩");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // リスナー設定
        manager.registerListener(this,
                stepCntSensor,
                SensorManager.SENSOR_DELAY_NORMAL);

        manager.registerListener(this,
                detectorSensor,
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


}
