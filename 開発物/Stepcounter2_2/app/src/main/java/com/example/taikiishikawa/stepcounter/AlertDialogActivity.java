package com.example.taikiishikawa.stepcounter;

/**
 * Created by Taiki Ishikawa on 2017/10/17.
 */

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class AlertDialogActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AlertDialogFragment fragment = new AlertDialogFragment();
        fragment.setCancelable(false);
        fragment.show(getSupportFragmentManager(), "alert_dialog");
    }
}