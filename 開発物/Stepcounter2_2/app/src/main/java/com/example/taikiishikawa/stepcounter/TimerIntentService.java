package com.example.taikiishikawa.stepcounter;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by Taiki Ishikawa on 2017/10/17.
 */

public class TimerIntentService extends IntentService {

    public TimerIntentService(String name) {
        super(name);
    }

    public TimerIntentService() {
        super("TimerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent data) {
        //SystemClock.sleep(5000);

        Intent intent = new Intent();
        intent.putExtra("alertMessage", data.getStringExtra("alertMessage"));
        intent.setAction("TIMER_FINISHED");
        sendBroadcast(intent);
    }
}
