package com.example.taikiishikawa.stepcounter;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

public class HttpResponseAsync extends AsyncTask<Float, Object, String> {

    private CallBackTask callbacktask;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // doInBackground前処理
    }

    @Override
    protected String doInBackground(Float... params) {
        HttpURLConnection con = null;
        URL url = null;
        String urlSt = "http://160.16.210.86:8080/taiki";

        Float light = params[0];
        Float weekday = params[1];
        Float time = params[2];
        Float latitude = params[3];
        Float longitude = params[4];

        try {
            // URLの作成
            url = new URL(urlSt);
            // 接続用HttpURLConnectionオブジェクト作成
            con = (HttpURLConnection) url.openConnection();
            // リクエストメソッドの設定
            con.setRequestMethod("POST");
            // リダイレクトを自動で許可しない設定
            con.setInstanceFollowRedirects(false);
            // URL接続からデータを読み取る場合はtrue
            con.setDoInput(true);
            // URL接続にデータを書き込む場合はtrue
            con.setDoOutput(true);

            // 接続
            con.connect(); // ①

            //ステップ5:リクエストボディの書き出しを行う。
            OutputStream outputStream = con.getOutputStream();
            HashMap<String, Object> jsonMap = new HashMap<>();
            jsonMap.put("light" , light);
            jsonMap.put("weekday" , weekday);
            jsonMap.put("time" , time);
            jsonMap.put("latitude" , latitude);
            jsonMap.put("longitude" , longitude);
            if (jsonMap.size() > 0) {
                //JSON形式の文字列に変換する。
                JSONObject responseJsonObject = new JSONObject(jsonMap);
                String jsonText = responseJsonObject.toString();
                PrintStream ps = new PrintStream(con.getOutputStream());
                ps.print(jsonText);
                ps.close();
            }
            outputStream.close();

            InputStream in = con.getInputStream();
            byte bodyByte[] = new byte[1024];
            in.read(bodyByte);
            in.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        InputStream in = null;
        try {
            in = con.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String readSt = null;
        try {
            if (in != null) {
                readSt = readInputStream(in);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return readSt;
    }

    public String readInputStream(InputStream in) throws IOException, UnsupportedEncodingException {
        StringBuffer sb = new StringBuffer();
        String st = "";

        BufferedReader br = new BufferedReader(new InputStreamReader(in, "UTF-8"));
        while ((st = br.readLine()) != null) {
            sb.append(st);
        }
        try {
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        callbacktask.CallBack(result);
    }

    public void setOnCallBack(CallBackTask _cbj) {
        callbacktask = _cbj;
    }

    public static class CallBackTask {
        public void CallBack(String result) {
        }
    }
}